<html>
    <head>
            <title>Sample Movie Application</title>
    </head>
    <body>
            <h1>List Movies</h1>


<cf_showsource>
<cfscript>
    // get the query from the MovieService
    qList = application.AppModel.MovieService.list();

    // convert the query to a struct
    out_local = convertQueryToStructWithDupValues(qList, 'id_movie');

    /**
     * Improved version of the 'convertQueryToStructWithDupValues' function within rcv.utils.batman
     *
            out_batman = application.AppModel.batman.convertQueryForJSONOutput(
                q: qList
                , rowkey: 'id_movie'
                , colsThatShouldBeAnArray : [ 
                    { 'col':'actrs',       'key':'id_actr' } // 	'delim':'|' optional default 
                    , { 'col':'categorys', 	'key':'id_cat', }]
                , returnAsArray: true
            );
    */

    writeDump({
          '1_asJson': serializeJSON(out_local)
        , '2_struct': out_local
        , '3_query': qList
        // , '4_out_batman': out_batman
    });

/*
=======================================================================================
== Requested example of modern CFML features ==========================================
=======================================================================================
*/
    public struct function convertQueryToStructWithDupValues(
        required query q
        , required string rowKey
        ) {
        var tmpRowKey = arguments.rowKey;
        return arguments.q.reduce(
            function(rsltStruct, currentRowStruct, i, qReference) {
                if(!structIsEmpty(rsltStruct)) {
                    var currentRowKey = currentRowStruct[tmpRowKey];
                    if( structKeyExists(rsltStruct, currentRowKey) ) {
                        // create a temp variable for the matched key/val
                        var tmpStruct = rsltStruct[currentRowKey];
                        // remove the rowKey from the currentRowStruct for simplicity
                        structDelete(currentRowStruct, tmpRowKey);
                        currentRowStruct.each(function(key,val,refToStruct){
                            // check to see if this row is a duplicate from the previous
                            if(isSimpleValue(tmpStruct[key])) {
                                // if not, set the value to an array containing the existing AND different value
                                if(tmpStruct[key] != val) {
                                    tmpStruct[key] = [tmpStruct[key], val];
                                }
                            }
                            // if an array is present, dup check, then add
                            else if(isArray(tmpStruct[key])) {
                                if(!arrayContainsNoCase(tmpStruct[key], val)) {
                                    arrayAppend(tmpStruct[key], val);
                                }
                            }
                        }
                        , true // execute in parallel
                        );
                    }
                    else {
                        structInsert(rsltStruct, currentRowStruct[tmpRowKey], currentRowStruct);
                    }
                }
                else {
                    structInsert(rsltStruct, currentRowStruct[tmpRowKey], currentRowStruct);
                }
                // ==============================
                // return struct to the next iteration
                return rsltStruct;
            }
            , {} // initial Value for rsltStruct
        );
    }
    


</cfscript>



</body>
</html>