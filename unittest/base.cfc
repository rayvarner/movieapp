component extends="mxunit.framework.TestCase" {

	private struct function buildModel() {
        return new movieapp.com.AppModel();
	}

	public any function beforeTests() {
		try {
			var AppModel = this.buildModel(); 
			// NOTE! The keys in AppModel are appended to the variables scope
			// so that AppModel.fooservice is actually in variables.fooservice in the unit tests
			structAppend(
				variables
				, AppModel.getModel()
				, true
			);
		}
		catch(any e) {
			var failMsg = '#getComponentMetaData(THIS).name#.beforeTests() | ' & e.message & ' ' & e.detail;
			writeLog(text=failMsg
					, type="error"
					, file="dev"
					);
			debug(failMsg);
			fail(failMsg);
		}
	}

}