﻿component {

	this.name = "rcvutils_testing_" & hash(getCurrentTemplatePath());
	this.applicationTimeout = createTimeSpan(0,0,2,0);
	this.sessionManagement = false;
	this.mappings['/mxunit'] = expandPath('/mxunit');
	this.datasource = 'MovieDB';

	public any function onApplicationStart() {
		application.factoryArgs = {};
		application['started']=now();
		writeLog(text='=========================================================', type='info', file='dev');
		writeLog(text='== onApplicationStart() #application.started# #this.name#', type='info', file='dev');
		return true;
	}

	public any function onApplicationEnd( required struct applicationScope ) {
		var started=arguments.applicationScope['started'];
		var appLength=TimeFormat(Now() - started, 'H:mm:ss');
		writeLog(text='== onApplicationEnd() Duration:#appLength#', type='info', file='dev');
	}

	public any function onRequestStart() {
		// Force cache to clear
		var headerTime = GetHttpTimeString(now());
		header name="Cache-Control" value="must-revalidate";
		header name="Pragma" value="no-cache";
		header name="Expires" value=headerTime;
		header name="Last-Modified" value=headerTime;
		// Clear the query cache
		objectcache action="clear";
		// clear the template cache
		pagePoolClear();
		// clear the component path cache
		componentCacheClear();
		// clear custom tag cache
		ctCacheClear();
	}

}