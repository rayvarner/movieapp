component extends="movieapp.Application"
{
		this.name = 'movieapp_vpi_' & Hash( GetCurrentTemplatePath() );
		this.applicationTimeout=createTimeSpan(0,0,10,0);
		this.sessionManagement=false;
		this.clientManagement=false;

		/** ===========================================================
		 * onError is NEVER called in a REST application?? TOO WEIRD!!!!
		 ==============================================================*/
		public any function onError(required any EventName, required any Exception) {
			writeLog(text="MOVIEAPP VPI onError() Handler Called??? WOW!!!! : #serializeJSON(arguments)#", type="FATAL", file="dev");
			super.onError(argumentCollection = arguments);
		}

		public any function onApplicationStart()
		{
			application.started=now();
			application.appModel = super.buildModel();
			application.AppModel.Logger.logMe("== MovieApp VPI onAppStart() #application.started#");
		}

		public any function onApplicationEnd(
			required struct AppScope
		) {
			try {
				var Logger = arguments.AppScope.AppModel.Logger;
				var Started = arguments.AppScope.started;
				var appLength=TimeFormat(Now() - Started, "H:mm:ss");
				Logger.logMe('== MovieApp VPI onAppEnd() length ' & appLength);
			}
			catch (any e) {
				writeLog(text = e.message & e.detail, type="error" );
				writeLog(text = structKeyList(arguments.AppScope));
			}
		}

		public boolean function onRequestStart(required string targetPage )
		{
			if (structKeyExists(url,'reload')) {
				lock scope="application" type="exclusive" timeout="30" {
					structDelete(application, 'AppModel');
				}
				lock scope="application" type="exclusive" timeout="30" {
					application.appModel = super.buildModel();
				}
				application.AppModel.Logger.logMe("== MovieApp VPI RELOADED");
				request.appreloaded = true;
			}

			try {
				request.Dumper = Application.AppModel.Dumper;
			}
			catch(any e) {
				request.Dumper = new rcv.utils.Dumper(
					writePath: expandPath('/movieapp/')
					, returnUrlPrefix: "#cgi.server_port_secure?'https':'http'#://#cgi.SERVER_NAME#:#cgi.SERVER_PORT#/movieapp/"
				);
				writeLog(text="MOVIEAPP VPI onRequestStart() error assigning the dumper : #e.message# #e.detail#", type="error", file="dev");
			}
			return true;
		}

}