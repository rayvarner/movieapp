component restpath="/moviedb" rest="true" {

    setting requesttimeout="190000";
    param name="variables.myCfcName" default="#getComponentMetaData(THIS).name#";

	if(!structKeyExists(variables, 'AppModel')) {
        lock scope="application" type="exclusive" timeout="30" {
            variables['AppModel'] = application.AppModel;
        }
    }
    
    /**
     * /rest/vpi/moviedb/cats
     */
    remote any function getCategorys() 
        httpmethod="get" 
        restpath="cats"
    {
        try {
            return {'rslt': variables.AppModel.CategoryService.listForRest() };
        }
        catch(any e) {
            var response={ 
                status=500,                                     // status 500 is an application error
                content="#serializeJSON(e)#",                   // returning the CF error as JSON in the body
                headers={ 
                    'Content-Type': 'application/json'
                    , 'X-Custom-Header': 'ScrapeError'          // the JS looks for this 'ScrapeError' value
                    , 'Content-Location': request.Dumper.dumpMe(e)  //The Content-Location header indicates an alternate location for the returned data.
                    } 
                };
            restSetResponse(response);
        }
    }

    /**
     * /rest/vpi/moviedb/list/foo
     * 
     * returns movies by category if provided
     */
    remote any function getMovies(
        string category='' restargsource="path"
    ) 
        httpmethod="get" 
        restpath="list/{category}"
    {
        try {
            return {'rslt': 
                variables.AppModel.MovieService.listForCategoryRest(listToArray(arguments.category, ','))
            };
        }
        catch(any e) {
            variables.AppModel.Logger.LogMe(e.message & '|' & e.detail, 'ERROR');
            var response={ 
                status=500,                                     // status 500 is an application error
                content="#serializeJSON(e)#",                   // returning the CF error as JSON in the body
                headers={ 
                    'Content-Type': 'application/json'
                    , 'X-Custom-Header': 'ScrapeError'          // the JS looks for this 'ScrapeError' value
                    , 'Content-Location': request.Dumper.dumpMe(e)  //The Content-Location header indicates an alternate location for the returned data.
                    } 
                };
            restSetResponse(response);
        }
    }

    function onMissingMethod(
        string MethodName
        , array MethodArguments
    ) {
        request.Dumper.dumpMe(arguments);
    }

}