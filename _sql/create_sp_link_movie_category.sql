Use MovieDB
GO

IF EXISTS (SELECT name
    FROM sysobjects
    WHERE name = N'sp_link_movie_category'
    AND type = 'P')
    DROP PROCEDURE sp_link_movie_category
GO

create procedure sp_link_movie_category (
	  @id_movie int
	, @id_cat int
)
as
begin

-- check to see if the value already exists
	declare @existing AS int=0

	select @existing = id_moviecategory
	from MovieCategory
	where id_movie = @id_movie
		AND id_cat = @id_cat

	-- if it exists, return the id
		if @existing > 0
		begin
			return 1
		end

	-- do the insert and return the new id
		BEGIN
			insert into MovieCategory(id_movie, id_cat) values(@id_movie, @id_cat)
			if @@ROWCOUNT = 1
				return 1
			else
				return 0
		END

end
go