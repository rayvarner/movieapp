use MovieDB
go

IF EXISTS (SELECT name
    FROM sysobjects
    WHERE name = N'sp_moviesWithPartners'
    AND type = 'P')
    DROP PROCEDURE sp_moviesWithPartners
GO

create procedure sp_moviesWithPartners (
	@namecompact varchar(50)
	-- if the namecompact2 section is enabled, the movies where a pairing occurrs are returned
	--, @namecompact2 varchar(50)=NULL
)
as
begin

-- Get movies where an actr has at least 1 pairing
select mov.id_movie, stu.studio_name, mov.movie_title, mov.dateimported
			, (	select count(idl2.id_actr) 
				from actr idl2 
				inner join Movieactr mi3 on idl2.id_actr = mi3.id_actr
				where mi3.id_movie = mov.id_movie
			) AS numactrs
		, actr.namecompact

	from Movie mov 
		inner join Studio stu 			on mov.id_studio = stu.id_studio
		left join Movieactr movactr		on mov.id_movie = movactr.id_movie
		left join actr actr				on actr.id_actr = movactr.id_actr
		where mov.id_movie IN
		(
			select movieid from
				(
				select mi1.id_movie as movieid from Movieactr mi1 where mi1.id_actr = 
					(select idl1.id_actr from actr idl1 where idl1.namecompact = @namecompact
						)
				) as qFirst


		-- second match (opt)
		/*
			inner join 	
				(
				select mi2.id_movieactr as movieactrid2, mi2.id_movie as movieid2, mi2.id_actr as actrid2 from Movieactr mi2 where mi2.id_actr = 
					(select id_actr from actr where namecompact = @namecompact2 
						)
				) as qOther
			on qFirst.movieid = qOther.movieid2
		*/
		-- /second match (opt)


		) 
	group by mov.id_movie, mov.movie_title, stu.studio_name, mov.dateimported, actr.namecompact
		HAVING (	select count(idl2.id_actr) 
					from actr idl2 
					inner join Movieactr mi3 on idl2.id_actr = mi3.id_actr
					where mi3.id_movie = mov.id_movie
				) > 1
	order by numactrs desc, studio_name, movie_title


	return 1


	
END
GO