
USE MovieDB
GO

ALTER TABLE Studio DROP CONSTRAINT uniq_studio_name;
ALTER TABLE Movie DROP CONSTRAINT fk_movie_studio;
ALTER TABLE Actr DROP CONSTRAINT uniq_namecompact;
ALTER TABLE Category DROP CONSTRAINT uniq_cat_name;
ALTER TABLE MovieCategory DROP CONSTRAINT fk_moviecategory_movie;
ALTER TABLE MovieCategory DROP CONSTRAINT fk_moviecategory_category;
ALTER TABLE MovieActr DROP CONSTRAINT fk_movieactr_movie;
ALTER TABLE MovieActr DROP CONSTRAINT fk_movieactr_actr;
ALTER TABLE GraphicFile DROP CONSTRAINT fk_graphicfile_movie;
ALTER TABLE GraphicFile DROP CONSTRAINT fk_graphicfile_actr;
ALTER TABLE VideoFile DROP CONSTRAINT fk_videofile_movie;
GO

TRUNCATE TABLE Studio;
TRUNCATE TABLE Movie;
TRUNCATE TABLE Actr;
TRUNCATE TABLE Category;
TRUNCATE TABLE MovieCategory;
TRUNCATE TABLE MovieActr;
TRUNCATE TABLE GraphicFile;
TRUNCATE TABLE VideoFile;
GO

ALTER TABLE Studio ADD CONSTRAINT uniq_studio_name UNIQUE(studio_name);
ALTER TABLE Movie ADD CONSTRAINT fk_movie_studio FOREIGN KEY (id_studio) REFERENCES Studio (id_studio);
ALTER TABLE Actr ADD CONSTRAINT uniq_namecompact UNIQUE(namecompact);
ALTER TABLE Category ADD CONSTRAINT uniq_cat_name UNIQUE(cat_name);
ALTER TABLE MovieCategory ADD CONSTRAINT fk_moviecategory_movie FOREIGN KEY (id_movie) REFERENCES Movie(id_movie);
ALTER TABLE MovieCategory ADD CONSTRAINT fk_moviecategory_category FOREIGN KEY (id_cat) REFERENCES Category(id_cat);
ALTER TABLE MovieActr ADD CONSTRAINT fk_movieactr_movie FOREIGN KEY (id_movie) REFERENCES Movie(id_movie);
ALTER TABLE MovieActr ADD CONSTRAINT fk_movieactr_actr FOREIGN KEY (id_actr) REFERENCES actr(id_actr);
ALTER TABLE GraphicFile ADD CONSTRAINT fk_graphicfile_movie FOREIGN KEY (id_movie) REFERENCES Movie(id_movie);
ALTER TABLE GraphicFile ADD CONSTRAINT fk_graphicfile_actr FOREIGN KEY (id_actr) REFERENCES actr(id_actr);
ALTER TABLE VideoFile ADD CONSTRAINT fk_videofile_movie FOREIGN KEY (id_movie) REFERENCES Movie(id_movie);
GO
