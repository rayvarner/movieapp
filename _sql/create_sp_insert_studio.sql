Use MovieDB
GO

IF EXISTS (SELECT name
    FROM sysobjects
    WHERE name = N'sp_insert_studio'
    AND type = 'P')
    DROP PROCEDURE sp_insert_studio
GO

create procedure sp_insert_studio (
	@studio_name varchar(255)
)
as
begin

	-- check to see if the value already exists
	declare @existing AS int=0

	select @existing = id_studio
	from Studio
	where studio_name = @studio_name

	-- if it exists, return the id
		if @existing > 1
		begin
			return @existing
		end

	-- do the insert and return the new id
		BEGIN
			insert into Studio (studio_name) values(@studio_name)
			if @@ROWCOUNT = 1
				return @@identity
			else
				return 0	
		END
	
END
GO