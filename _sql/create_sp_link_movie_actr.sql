Use MovieDB
GO

IF EXISTS (SELECT name
    FROM sysobjects
    WHERE name = N'sp_link_movie_actr'
    AND type = 'P')
    DROP PROCEDURE sp_link_movie_actr
GO

create procedure sp_link_movie_actr (
	  @id_movie int
	, @id_actr int
)
as
begin

-- check to see if the value already exists
	declare @existing AS int=0

	select @existing = id_movieactr
	from Movieactr
	where id_movie = @id_movie
		AND id_actr = @id_actr

	-- if it exists, return the id
		if @existing > 0
		begin
			return 1
		end

	-- do the insert and return the new id
		BEGIN
			insert into Movieactr(id_movie, id_actr) values(@id_movie, @id_actr)
			if @@ROWCOUNT = 1
				return 1
			else
				return 0
		END

end
go