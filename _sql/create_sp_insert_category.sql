Use MovieDB
GO

IF EXISTS (SELECT name
    FROM sysobjects
    WHERE name = N'sp_insert_category'
    AND type = 'P')
    DROP PROCEDURE sp_insert_category
GO


create procedure sp_insert_category (
	@cat_name varchar(100)
)
as
begin

	-- check to see if the value already exists
	declare @existing AS int=0

	select @existing = id_cat
	from Category
	where cat_name = @cat_name

	-- if it exists, return the id
		if @existing > 1
		begin
			select 'id_cat' = @existing
			return 0
		end

	-- do the insert and return the new id
		BEGIN
			insert into Category (cat_name) 
			values(@cat_name)
			select 'id_cat' = @@IDENTITY
			return 1
		END
	
END
GO