
USE MovieDB
GO

/*=======================================
== Drop constraints, procs, and tables
========================================= */

ALTER TABLE Studio DROP CONSTRAINT uniq_studio_name;							-- sysobjects.type='K'
ALTER TABLE Movie DROP CONSTRAINT fk_movie_studio;						-- sysobjects.type='F'
ALTER TABLE Actr DROP CONSTRAINT uniq_namecompact;							-- sysobjects.type='K'
ALTER TABLE Category DROP CONSTRAINT uniq_cat_name;							-- sysobjects.type='K'
ALTER TABLE MovieCategory DROP CONSTRAINT fk_moviecategory_movie;			-- sysobjects.type='F'
ALTER TABLE MovieCategory DROP CONSTRAINT fk_moviecategory_category;		-- sysobjects.type='F'
ALTER TABLE MovieActr DROP CONSTRAINT fk_movieactr_movie;					-- sysobjects.type='F'
ALTER TABLE MovieActr DROP CONSTRAINT fk_movieactr_actr;					-- sysobjects.type='F'
ALTER TABLE GraphicFile DROP CONSTRAINT fk_graphicfile_movie;				-- sysobjects.type='F'
ALTER TABLE GraphicFile DROP CONSTRAINT fk_graphicfile_actr;				-- sysobjects.type='F'
ALTER TABLE VideoFile DROP CONSTRAINT fk_videofile_movie;					-- sysobjects.type='F'
GO

DROP PROCEDURE sp_insert_category;		-- sysobjects.type='P'
DROP PROCEDURE sp_insert_actr;			-- sysobjects.type='P'
DROP PROCEDURE sp_insert_movie;			-- sysobjects.type='P'
DROP PROCEDURE sp_insert_studio;			-- sysobjects.type='P'
DROP PROCEDURE sp_link_movie_category;	-- sysobjects.type='P'
DROP PROCEDURE sp_link_movie_actr;		-- sysobjects.type='P'
GO

DROP TABLE Studio;			-- sysobjects.type='U'
DROP TABLE Movie;				-- sysobjects.type='U'
DROP TABLE Actr;				-- sysobjects.type='U'
DROP TABLE Category;			-- sysobjects.type='U'
DROP TABLE MovieCategory;		-- sysobjects.type='U'
DROP TABLE MovieActr;			-- sysobjects.type='U'
DROP TABLE GraphicFile;		-- sysobjects.type='U'
DROP TABLE VideoFile;			-- sysobjects.type='U'
GO

/*=======================================
== Create tables & constraints
========================================= */



create table Studio(
    id_studio INT PRIMARY KEY IDENTITY (1000,1),
    studio_name varchar(255),
    CONSTRAINT uniq_studio_name UNIQUE(studio_name)
);

create table Movie(
    id_movie INT PRIMARY KEY IDENTITY (1000,1),
    movie_title nVARCHAR(255),
    id_studio INT NOT NULL,
    dateimported date DEFAULT GETDATE(),
    meta_vr varchar(255),
    dateproduced date
    , CONSTRAINT fk_movie_studio FOREIGN KEY (id_studio) REFERENCES Studio (id_studio)
);

create table Actr(
    id_actr INT PRIMARY KEY IDENTITY (1000,1),
    namecompact nvarchar(50) not null,
    nameexpanded nvarchar(100),
    namealias1 nvarchar(100),
    namealias2 nvarchar(100)
    , CONSTRAINT uniq_namecompact UNIQUE(namecompact)
);

create table Category(
    id_cat INT PRIMARY KEY IDENTITY (1000,1),
    cat_name varchar(100) not null
    , CONSTRAINT uniq_cat_name UNIQUE(cat_name)
);

create table MovieCategory(
    id_moviecategory INT PRIMARY KEY IDENTITY (1000,1),
    id_movie INT NOT NULL,
    id_cat INT NOT NULL
    , CONSTRAINT fk_moviecategory_movie FOREIGN KEY (id_movie) REFERENCES Movie(id_movie)
    , CONSTRAINT fk_moviecategory_category FOREIGN KEY (id_cat) REFERENCES Category(id_cat)
);

create table MovieActr(
    id_movieactr INT PRIMARY KEY IDENTITY (1000,1),
    id_movie INT NOT NULL,
    id_actr INT NOT NULL
    , CONSTRAINT fk_movieactr_movie FOREIGN KEY (id_movie) REFERENCES Movie(id_movie)
    , CONSTRAINT fk_movieactr_actr FOREIGN KEY (id_actr) REFERENCES actr(id_actr)
);

create table GraphicFile(
    id_graphicfile INT PRIMARY KEY IDENTITY (1000,1),
    graphic varchar(max) not null,
    graphic_small varchar(max),
    id_movie INT,
    id_actr INT
    , CONSTRAINT fk_graphicfile_movie FOREIGN KEY (id_movie) REFERENCES Movie(id_movie)
    , CONSTRAINT fk_graphicfile_actr FOREIGN KEY (id_actr) REFERENCES actr(id_actr)
);

create table VideoFile(
    id_videofile INT PRIMARY KEY IDENTITY (1000,1),
    id_movie INT NOT NULL,
    video_name varchar(max) not null,
    location_current varchar(255),
    location_archive varchar(255)
    , CONSTRAINT fk_videofile_movie FOREIGN KEY (id_movie) REFERENCES Movie(id_movie)
);

/*==========================================================
== NOTE: You will still need to create the stored procs!
============================================================ */