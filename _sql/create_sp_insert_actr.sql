Use MovieDB
GO 

IF EXISTS (SELECT name
    FROM sysobjects
    WHERE name = N'sp_insert_actr'
    AND type = 'P')
    DROP PROCEDURE sp_insert_actr
GO

create procedure sp_insert_actr (
	@namecompact varchar(50)
	, @nameexpanded varchar(100)=NULL
	, @namealias1 nvarchar(100)=NULL
	, @namealias2 nvarchar(100)=NULL
)
as
begin

	-- check to see if the value already exists
	declare @existing AS int=0

	select @existing = id_actr
	from Actr
	where namecompact = @namecompact

	-- if it exists, return the id
		if @existing > 1
		begin
			select 'id_actr' = @existing
			return 0
		end

	-- do the insert and return the new id
		BEGIN
			insert into Actr (namecompact,nameexpanded,namealias1,namealias2) 
			values(@namecompact, @nameexpanded, @namealias1, @namealias2)

				select 'id_actr' = @@IDENTITY
				return 1
		END
	
END
GO