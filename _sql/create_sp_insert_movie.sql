Use MovieDB
GO

IF EXISTS (SELECT name
    FROM sysobjects
    WHERE name = N'sp_insert_movie'
    AND type = 'P')
    DROP PROCEDURE sp_insert_movie
GO

create procedure sp_insert_movie (
	  @movie_title varchar(255)
    , @studio_name varchar(255)
	, @video_name varchar(max)
	, @graphic varchar(max)
	, @dateimported date
	, @meta_vr varchar(255) = NULL
	, @dateproduced date = NULL
	, @location_current varchar(255) = NULL
	, @location_archive varchar(255) = NULL
	, @graphic_small varchar(100) = NULL
)
as
begin


-- get the id_studio for the studio_name passed in 
    DECLARE @id_studio int
    EXECUTE @id_studio = sp_insert_studio @studio_name
    
	-- start the insert
	insert into Movie (movie_title, id_studio, dateimported, meta_vr, dateproduced)
	values(@movie_title, @id_studio, @dateimported, @meta_vr, @dateproduced)


	DECLARE @id_movie int = @@IDENTITY

	-- GraphicFile
		-- dup check
			declare @existingGraphicID AS int=0
			select @existingGraphicID = id_graphicfile from GraphicFile where GraphicFile.id_movie = @id_movie
			if @existingGraphicID > 0
				update GraphicFile set graphic = @graphic where id_graphicfile = @existingGraphicID
			else
				insert into GraphicFile (graphic, id_movie) VALUES(@graphic, @id_movie)
			
	-- VideoFile
		-- dup check
			declare @existingVideoID AS int=0
			select @existingVideoID = id_videofile from VideoFile where VideoFile.id_movie = @id_movie
			if @existingVideoID > 0
				update VideoFile 
				set video_name = @video_name, location_current = @location_current 
				where id_videofile = @existingVideoID
			else
				insert into VideoFile (video_name, location_current, id_movie) VALUES(@video_name, @location_current, @id_movie)

		select 'id_movie' = @id_movie
end
go