component 
{
		this.name = 'movieapp_' & Hash( GetCurrentTemplatePath() );
		this.applicationTimeout=createTimeSpan(0,1,0,0);
		this.sessionManagement=true;
		this.sessionTimeout=createTimeSpan(0,0,30,0);
		this.datasource = 'MovieDB';

		public any function onApplicationStart()
		{
			application.started=now();
			application.AppModel = this.buildModel();
			application.AppModel.Logger.logMe("== onAppStart() #application.started#");
		}

		public any function onApplicationEnd(
			required struct AppScope
		) {
			try {
				var Logger = arguments.AppScope.AppModel.Logger;
				var Started = arguments.AppScope.started;
				Logger.logMe('== onAppEnd() length ' & TimeFormat(Now() - Started, "H:mm:ss"));
			}
			catch (any e) {
				writeLog(text = e.message & e.detail, type="error" );
				writeLog(text = structKeyList(arguments.AppScope));
			}
		}

		public boolean function onRequestStart(required string targetPage )
		{
			if (structKeyExists(url,'reload')) {
				lock scope="application" type="exclusive" timeout="30" {
					structDelete(application, 'AppModel');
				}
				lock scope="application" type="exclusive" timeout="30" {
					application.appModel = this.buildModel();
				}
			}
			return true;
		}

		public any function onError(required any Exception, required any EventName) {
			writeDump(arguments);
			abort;
		}

		public struct function buildModel() {
			return new movieapp.com.AppModel().getModel();
		}
		
}