<!---
	showsource custom tag - add the following to Application.cfc:
	
	<cffunction name="onRequestEnd">
		<cfargument name="targetPage" />
		
		<cfif right(arguments.targetPage,4) is ".cfm">
			<cf_showsource>
		</cfif>
		
	</cffunction>
--->
<cfif thisTag.ExecutionMode is "end">
	<cfexit method="exittag" />
</cfif>

<!--- START: SyntaxHighlighter 
http://alexgorbatchev.com/SyntaxHighlighter/manual/installation.html 
	note the class on the <pre> tag <pre: class="brush: cf">
--->
<cfsavecontent variable="syntaxHighlighter" >
<script src="/nwd/libs/syntaxhighlighter_3.0.83/scripts/shCore.js" type="text/javascript"></script>
<script src="/nwd/libs/syntaxhighlighter_3.0.83/scripts/shBrushColdFusion.js" type="text/javascript"></script>
<link href="/nwd/libs/syntaxhighlighter_3.0.83/styles/shCore.css" rel="stylesheet" type="text/css" />
<link href="/nwd/libs/syntaxhighlighter_3.0.83/styles/shCoreRDark.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
     SyntaxHighlighter.all();
</script>
</cfsavecontent>
<cfhtmlhead text="#syntaxHighlighter#" />
<!--- END: SyntaxHighlighter --->


<div id="sourcecodecontainer">
	<script type="text/javascript">
		function toggleSourceCode() {
			var scArea = document.getElementById("sourcecodearea");
			var scLink = document.getElementById("sourcecodelink");
			if (scArea.style.display == "none") {
				scArea.style.display = "";
				scLink.innerHTML = "Hide Source Code";
			} else {
				scArea.style.display = "none";
				scLink.innerHTML = "Show Source Code";
			}
			return false;
		}
	</script>
	<div><a href="#" onclick="return toggleSourceCode();"><span id="sourcecodelink">Show Source Code</span></a></div>
	<div id="sourcecodearea" style="display: none;">
		<cffile action="read" file="#getBaseTemplatePath()#" variable="sourcecode" />
		<pre class="brush: cf"><cfoutput>#htmlEditFormat(sourcecode)#</cfoutput></pre>
	</div>
</div>


