

<cfscript>



	moviesWithActrs = [
		{
			'available':''
			, 'categorys': [{'name': 'adventure'}, {'name': 'comedy'}, {'name': 'musical'}]
			, 'dir': 'Z:\movies'
			, 'file': 'Pick of Destiny cover.jpg'
			, 'actrs': [{'nameCompact': 'JackBlack'}, {'nameCompact': 'KyleGas'}]
			, 'meta': '1080p subtitles'
			, 'name': 'Tenacious D - The Pick of Destiny'
			, 'studio': {'name': 'Warner Bros'}
			, 'webdir': '/movies'
		}
		, {
			'available':''
			, 'categorys': [{'name': 'adventure'}, {'name': 'sci-fi'}, {'name': 'family'}]
			, 'dir': 'Z:\movies'
			, 'file': 'Star Wars 4 cover.jpg'
			, 'actrs': [{'nameCompact': 'HarrisonFord'}, {'nameCompact': 'CarrieFisher'}]
			, 'meta': '1080p subtitles'
			, 'name': 'Star Wars - Episode IV A New Hope'
			, 'studio': {'name': 'Universal'}
			, 'webdir': '/movies'
		}
		, {
			'available':''
			, 'categorys': [{'name': 'adventure'}, {'name': 'family'}]
			, 'dir': 'Z:\movies'
			, 'dateProduced': '12/31/1989'
			, 'file': 'Indiana Jones 3 cover.jpg'
			, 'actrs': [{'nameCompact': 'HarrisonFord'}, {'nameCompact': 'SeanConnery'}]
			, 'meta': '720p'
			, 'name': 'Indiana Jones and the Last Crusade'
			, 'studio': {'name': 'Universal'}
			, 'webdir': '/movies'
		}
	];

	appModel = application.appModel;
	MovieService = appModel.MovieService;

	out = {
		'fails':[]
		, 'totalRecords': arrayLen(moviesWithActrs)
		, 'successes': 0
	};

	ingestDefaults = {
		'dateProduced':'12/31/1999'
		, 'location_current':''
		, 'categorys':[]
		, 'dateImported': dateFormat(now(), 'MM/DD/YYYY')
	};




	for(mVal in moviesWithActrs) {
		
		success=false;

		Movie = MovieService.new( duplicate(ingestDefaults) );
		Movie.setTitle( mVal.name );
		Movie.setGraphic(mVal.file);
		Movie.setMeta_VR(mVal.meta);
		Movie.setStudio(mVal.Studio);

		// remove the ' cover.jpg' and replace with .mp4 as per the file naming convention
		Movie.setvideo_name(  
			(mid(mVal.file, 1, len(mVal.file)-10 )) & '.mp4'  
		);

		/* 	== add an actr one by one if you choose
			for(actr in mVal.actrs) {
				Movie.AddActrs( {'nameCompact':actr} );
			}
		*/

		// == add all the actrs at once
		Movie.setActrs(mVal.actrs);
		// == add all categorys at once
		Movie.setCategorys(mVal.categorys);

		try {
			// MovieService.save() returns boolean
			if(!MovieService.save(Movie)) {
				throw('Movie did not save');
			} 
			out.successes++;
		}
		catch(any e) {
			arrayAppend( out.fails, '#e.message# #e.detail# | ' & serializeJson(Movie.getMemento()) );	
		}

	}

    writeDump(out);
    abort;
</cfscript>