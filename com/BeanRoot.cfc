﻿component 
	output="false" 
	displayname="BeanRoot" 
	hint="I provide basic property validation based on the extended components required properties"
{
	property name="Logger" type="rcv.utils.Logger";

	/**
	* When passed a struct, I loop over the internal properties to verify existence
	* If a key does not exist I throw an exception
	*/
	public void function setMemento(
		required struct s
	) {
		var errors=[];
		// verify there is a setter for every key in the struct
		for(var key in arguments.s) {
			var rslts = structFindValue(getMetaData(this), key, 'all');
			if(!arrayLen( rslts )) {
				arrayAppend(errors, key);
			}
		}
		if(arrayLen(errors)) {
			variables.logger.logMe("-- Errors with BeanRoot.setMemento(): " & arrayToList(errors), 'error');
		}
		else {
			// set the memento
			variables['memento']=duplicate(arguments.s);
		}
	}

	private any function arrayOfStructsToArrayOfObjects(required array dataArray, required string dotPathToCFC) {
		var ret = [];
		var obj = '';
		if(isArray(arguments.dataArray) && arrayLen(arguments.dataArray) >= 1) {
			for(var item in arguments.dataArray) {
				
				if(isStruct(item)) {
					arrayAppend(ret, structToObject(item, arguments.dotPathToCFC));
				}
				else {
					return arguments.dataArray;
				}
			}
		}
		else {
				ret = arguments.dataArray;
		}
		return ret;	
	}


	/**
	*	I loop over the component's property metadata.
	* 	If a property is required, I check the variables scope for existence 
	* 		and validate the datatype based on the component metadata.
	* 
	* 	If validation is successful, an empty array will be returned.
	* 
	* 	Checks for Existence, DataType (opt), Empty string (opt)
	*/	
	public array function Validate(
			boolean checkDataType = true
			, boolean allowEmptyStringForRequiredProperty = false
		) {
		var ret = [];
		var myMD = getMetaData(this);
		try {		
			for(var prop in myMD.extends.properties) {
				if( structKeyExists(prop, 'required') && prop.required==true) {
					if(structKeyExists(variables.memento, prop.name)) {
						if(arguments.checkDataType) {
							// verify the prop.type is available to the isValid() function
							if(listContains('array,binary,boolean,date,guid,numeric,query,string,struct,UUID', prop.type)) {
							// now test for datatype
								if(isValid(prop.type, variables.memento[prop.name])) {
									if(!arguments.allowEmptyStringForRequiredProperty) {
										
										// check for a zero-length string
										if(prop.type == 'string' && !len(trim(variables.memento[prop.name]))) {
											arrayAppend(ret, prop.name & ' should not be a zero length string.');
										}
									}
								} 
								else {
									// property is the wrong type
									arrayAppend(ret, prop.name & ' exists but is the wrong datatype.');
								}
							}//if listContains
						}
					}
					else {
						// the property is not present
						arrayAppend(ret, prop.name & ' is not present in the variables scope.');
					}	
				}
			}// /for
		}// /try
		catch(any e) {
			arrayAppend(ret, e.message & e.detail);
		}	
		return ret;
	}	
	
	public struct function getMemento(){
		return variables.memento;
	}

}