 component accessors=true output=false persistent=false {

	public movieapp.com.AppModel function init() {
		structAppend(variables, arguments, true);
		return this;
	}

	public struct function getModel() {
		var appmodel = {};

		appmodel.batman = new rcv.utils.batman();
		appmodel.Logger = new rcv.utils.Logger();
		appmodel.Dumper = new rcv.utils.Dumper();

		appmodel.MovieService = new movieapp.com.model.services.MovieService(
			Logger: appmodel.Logger
			, batman: appmodel.batman
		);
		appmodel.actrservice = new movieapp.com.model.services.ActrService(
			Logger: appmodel.Logger
		);
		appmodel.StudioService = new movieapp.com.model.services.StudioService(
			Logger: appmodel.Logger
		);
		appmodel.CategoryService = new movieapp.com.model.services.CategoryService(
			Logger: appmodel.Logger
			, batman: appmodel.batman
		);
		return appmodel;
	}

}