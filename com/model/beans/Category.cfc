component extends="movieapp.com.BeanRoot" {
	
	property name="ID" type="numeric" hint="java.lang.Double";
	property name="MOVIES" type="array" hint="lucee.runtime.type.ArrayImpl";
	property name="NAME" type="string" hint="java.lang.String";
	
// init() ========================================================================

	private any function init(struct myStruct) {
		variables.memento={};
		if(structKeyExists(arguments, 'myStruct') && !structIsEmpty(arguments.myStruct)) {
			super.setMemento(arguments.myStruct);
		}
		return this;
	}
	
	

// ID ===============================================================

	public numeric function getID() {
		if(hasID()) {
			return variables.memento['ID'];
		}
	}
	public void function setID(
		required numeric mVal
	) {
		variables.memento['ID'] = arguments.mVal;
	}
	

	public boolean function hasID() {
		return structKeyExists(variables.memento, 'ID');
	}
	
// MOVIES ===============================================================

	public array function getMOVIES() {
		var ret=[];
		if(hasMOVIES() && arrayLen(variables.memento['MOVIES'])) {
			for(var MOVIESStruct IN variables.memento['MOVIES']) {
				arrayAppend(ret, new Movie(MOVIESStruct));
			}
		}
		return ret;
	}
	
	public void function addMOVIES(
		required any mVal
	) {
		if(!hasMOVIES()) {
			variables.memento['MOVIES']=[];	
		}	
		if(isObject(arguments.mVal)) {
			arrayAppend(variables.memento['MOVIES'], arguments.mVal.getMemento());
		}
		else {
			arrayAppend(variables.memento['MOVIES'], arguments.mVal);
		}
	}
	
	public void function setMOVIES(
		required array mVal
	) {
		if(arrayLen(arguments.mVal)) {
			if(!isObject(arguments.mVal[1])) {
				variables.memento['MOVIES'] = arguments.mVal;		
			}
			else 
			{ // loop over the array of objects and add the memento of each
				variables.memento['MOVIES']=[];
				for(var item in arguments.mVal) {
					arrayAppend(variables.memento['MOVIES'], item.getMemento());
				}
			}
		}
	}
	
	

	public boolean function hasMOVIES() {
		return structKeyExists(variables.memento, 'MOVIES');
	}
	
// NAME ===============================================================

	public string function getNAME() {
		if(hasNAME()) {
			return variables.memento['NAME'];
		}
	}
	public void function setNAME(
		required string mVal
	) {
		variables.memento['NAME'] = arguments.mVal;
	}
	

	public boolean function hasNAME() {
		return structKeyExists(variables.memento, 'NAME');
	}
	

}
