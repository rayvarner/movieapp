component extends="movieapp.com.BeanRoot" {
	
	property name="ALIAS1" type="string" hint="java.lang.String";
	property name="ALIAS2" type="string" hint="java.lang.String";
	property name="ID" type="numeric" hint="java.lang.Double";
	property name="MOVIES" type="array" hint="lucee.runtime.type.ArrayImpl";
	property name="NAMECOMPACT" type="string" hint="java.lang.String";
	property name="NAMEEXPANDED" type="string" hint="java.lang.String";
	
// init() ========================================================================

	private any function init(struct myStruct) {
		variables.memento={};
		if(structKeyExists(arguments, 'myStruct') && !structIsEmpty(arguments.myStruct)) {
			super.setMemento(arguments.myStruct);
		}
		return this;
	}
	
	

// ALIAS1 ===============================================================

	public string function getALIAS1() {
		if(hasALIAS1()) {
			return variables.memento['ALIAS1'];
		}
	}
	public void function setALIAS1(
		required string mVal
	) {
		variables.memento['ALIAS1'] = arguments.mVal;
	}
	

	public boolean function hasALIAS1() {
		return structKeyExists(variables.memento, 'ALIAS1');
	}
	
// ALIAS2 ===============================================================

	public string function getALIAS2() {
		if(hasALIAS2()) {
			return variables.memento['ALIAS2'];
		}
	}
	public void function setALIAS2(
		required string mVal
	) {
		variables.memento['ALIAS2'] = arguments.mVal;
	}
	

	public boolean function hasALIAS2() {
		return structKeyExists(variables.memento, 'ALIAS2');
	}
	
// ID ===============================================================

	public numeric function getID() {
		if(hasID()) {
			return variables.memento['ID'];
		}
	}
	public void function setID(
		required numeric mVal
	) {
		variables.memento['ID'] = arguments.mVal;
	}
	

	public boolean function hasID() {
		return structKeyExists(variables.memento, 'ID');
	}
	
// MOVIES ===============================================================

	public array function getMOVIES() {
		var ret=[];
		if(hasMOVIES() && arrayLen(variables.memento['MOVIES'])) {
			for(var MOVIESStruct IN variables.memento['MOVIES']) {
				arrayAppend(ret, new Movie(MOVIESStruct));
			}
		}
		return ret;
	}
	
	public void function addMOVIES(
		required any mVal
	) {
		if(!hasMOVIES()) {
			variables.memento['MOVIES']=[];	
		}	
		if(isObject(arguments.mVal)) {
			arrayAppend(variables.memento['MOVIES'], arguments.mVal.getMemento());
		}
		else {
			arrayAppend(variables.memento['MOVIES'], arguments.mVal);
		}
	}
	
	public void function setMOVIES(
		required array mVal
	) {
		if(arrayLen(arguments.mVal)) {
			if(!isObject(arguments.mVal[1])) {
				variables.memento['MOVIES'] = arguments.mVal;		
			}
			else 
			{ // loop over the array of objects and add the memento of each
				variables.memento['MOVIES']=[];
				for(var item in arguments.mVal) {
					arrayAppend(variables.memento['MOVIES'], item.getMemento());
				}
			}
		}
	}
	
	

	public boolean function hasMOVIES() {
		return structKeyExists(variables.memento, 'MOVIES');
	}
	
// NAMECOMPACT ===============================================================

	public string function getNAMECOMPACT() {
		if(hasNAMECOMPACT()) {
			return variables.memento['NAMECOMPACT'];
		}
	}
	public void function setNAMECOMPACT(
		required string mVal
	) {
		variables.memento['NAMECOMPACT'] = arguments.mVal;
	}
	

	public boolean function hasNAMECOMPACT() {
		return structKeyExists(variables.memento, 'NAMECOMPACT');
	}
	
// NAMEEXPANDED ===============================================================

	public string function getNAMEEXPANDED() {
		if(hasNAMEEXPANDED()) {
			return variables.memento['NAMEEXPANDED'];
		}
	}
	public void function setNAMEEXPANDED(
		required string mVal
	) {
		variables.memento['NAMEEXPANDED'] = arguments.mVal;
	}
	

	public boolean function hasNAMEEXPANDED() {
		return structKeyExists(variables.memento, 'NAMEEXPANDED');
	}
	

}
