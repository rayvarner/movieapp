component extends="movieapp.com.BeanRoot" {
	
	property name="CATEGORYS" type="array" hint="lucee.runtime.type.ArrayImpl";
	property name="DATEPRODUCED" type="date" hint="lucee.runtime.type.dt.DateTimeImpl";
	property name="ID" type="numeric" hint="java.lang.Double";
	property name="Actrs" type="array" hint="lucee.runtime.type.ArrayImpl";
	property name="META_VR" type="string" hint="java.lang.String";
	property name="STUDIO" type="struct" hint="lucee.runtime.type.StructImpl";
	property name="TITLE" type="string" hint="java.lang.String";
	property name="video_name" type="string";
	property name="location_current" type="string";
	property name="location_archive" type="string";
	property name="graphic" type="string";
	property name="dateImported" type="date";

	
// init() ========================================================================

	private any function init(
		struct myStruct
	) {
		variables.memento={};
		if(structKeyExists(arguments, 'myStruct') && !structIsEmpty(arguments.myStruct)) {
			super.setMemento(arguments.myStruct);
		}
		return this;
	}
	
	

// CATEGORYS ===============================================================

	public array function getCATEGORYS() {
		var ret=[];
		if(hasCATEGORYS() && arrayLen(variables.memento['CATEGORYS'])) {
			for(var CATEGORYSStruct IN variables.memento['CATEGORYS']) {
				arrayAppend(ret, new CATEGORY(CATEGORYSStruct));
			}
		}
		return ret;
	}
	
	public void function addCATEGORYS(
		required any mVal
	) {
		if(!hasCATEGORYS()) {
			variables.memento['CATEGORYS']=[];	
		}	
		if(isObject(arguments.mVal)) {
			arrayAppend(variables.memento['CATEGORYS'], arguments.mVal.getMemento());
		}
		else {
			arrayAppend(variables.memento['CATEGORYS'], arguments.mVal);
		}
	}
	
	public void function setCATEGORYS(
		required array mVal
	) {
		if(arrayLen(arguments.mVal)) {
			if(!isObject(arguments.mVal[1])) {
				variables.memento['CATEGORYS'] = arguments.mVal;		
			}
			else 
			{ // loop over the array of objects and add the memento of each
				variables.memento['CATEGORYS']=[];
				for(var item in arguments.mVal) {
					arrayAppend(variables.memento['CATEGORYS'], item.getMemento());
				}
			}
		}
	}
	
	

	public boolean function hasCATEGORYS() {
		return structKeyExists(variables.memento, 'CATEGORYS');
	}
	
// DATEPRODUCED ===============================================================

	public date function getDATEPRODUCED() {
		if(hasDATEPRODUCED()) {
			return variables.memento['DATEPRODUCED'];
		}
	}
	public void function setDATEPRODUCED(
		required date mVal
	) {
		variables.memento['DATEPRODUCED'] = arguments.mVal;
	}
	

	public boolean function hasDATEPRODUCED() {
		return structKeyExists(variables.memento, 'DATEPRODUCED');
	}
	
// ID ===============================================================

	public numeric function getID() {
		if(hasID()) {
			return variables.memento['ID'];
		}
	}
	public void function setID(
		required numeric mVal
	) {
		variables.memento['ID'] = arguments.mVal;
	}
	

	public boolean function hasID() {
		return structKeyExists(variables.memento, 'ID');
	}
	
// Actrs ===============================================================

	public array function getActrs() {
		var ret=[];
		if(hasActrs() && arrayLen(variables.memento['Actrs'])) {
			for(var ActrsStruct IN variables.memento['Actrs']) {
				arrayAppend(ret, new Actr(ActrsStruct));
			}
		}
		return ret;
	}
	
	public void function addActrs(
		required any mVal
	) {
		if(!hasActrs()) {
			variables.memento['Actrs']=[];	
		}	
		if(isObject(arguments.mVal)) {
			arrayAppend(variables.memento['Actrs'], arguments.mVal.getMemento());
		}
		else {
			arrayAppend(variables.memento['Actrs'], arguments.mVal);
		}
	}
	
	public void function setActrs(
		required array mVal
	) {
		if(arrayLen(arguments.mVal)) {
			if(!isObject(arguments.mVal[1])) {
				variables.memento['Actrs'] = arguments.mVal;		
			}
			else 
			{ // loop over the array of objects and add the memento of each
				variables.memento['Actrs']=[];
				for(var item in arguments.mVal) {
					arrayAppend(variables.memento['Actrs'], item.getMemento());
				}
			}
		}
	}
	
	

	public boolean function hasActrs() {
		return structKeyExists(variables.memento, 'Actrs');
	}

// META_VR ===============================================================

	public string function getMETA_VR() {
		if(hasMETA_VR()) {
			return variables.memento['META_VR'];
		}
	}
	public void function setMETA_VR(
		required string mVal
	) {
		variables.memento['META_VR'] = arguments.mVal;
	}
	

	public boolean function hasMETA_VR() {
		return structKeyExists(variables.memento, 'META_VR');
	}
	
// STUDIO ===============================================================

	public struct function getSTUDIO() {
		if(hasSTUDIO()) {
			return new Studio(variables.memento['STUDIO']);
		}
	}
	
	public void function setSTUDIO(
		required struct mVal
	) {
		variables.memento['STUDIO'] = arguments.mVal;
	}
	

	public boolean function hasSTUDIO() {
		return structKeyExists(variables.memento, 'STUDIO');
	}
	
// TITLE ===============================================================

	public string function getTITLE() {
		if(hasTITLE()) {
			return variables.memento['TITLE'];
		}
	}
	public void function setTITLE(
		required string mVal
	) {
		variables.memento['TITLE'] = arguments.mVal;
	}
	

	public boolean function hasTITLE() {
		return structKeyExists(variables.memento, 'TITLE');
	}
		
	// video_name ===============================================================

	public string function getvideo_name() {
		if(hasvideo_name()) {
			return variables.memento['video_name'];
		}
	}
	public void function setvideo_name(
		required string mVal
	) {
		variables.memento['video_name'] = arguments.mVal;
	}


	public boolean function hasvideo_name() {
		return structKeyExists(variables.memento, 'video_name');
	}

	// location_current ===============================================================

	public string function getlocation_current() {
		if(haslocation_current()) {
			return variables.memento['location_current'];
		}
	}
	public void function setlocation_current(
		required string mVal
	) {
		variables.memento['location_current'] = arguments.mVal;
	}


	public boolean function haslocation_current() {
		return structKeyExists(variables.memento, 'location_current');
	}

	// location_archive ===============================================================

	public string function getlocation_archive() {
		if(haslocation_archive()) {
			return variables.memento['location_archive'];
		}
	}
	public void function setlocation_archive(
		required string mVal
	) {
		variables.memento['location_archive'] = arguments.mVal;
	}


	public boolean function haslocation_archive() {
		return structKeyExists(variables.memento, 'location_archive');
	}

	// graphic ===============================================================

	public string function getgraphic() {
		if(hasgraphic()) {
			return variables.memento['graphic'];
		}
	}
	public void function setgraphic(
		required string mVal
	) {
		variables.memento['graphic'] = arguments.mVal;
	}


	public boolean function hasgraphic() {
		return structKeyExists(variables.memento, 'graphic');
	}

// DATEIMPORTED ===============================================================

	public date function getDateImported() {
		if(hasDateImported()) {
			return variables.memento['DateImported'];
		}
	}
	public void function setDateImported(
		required date mVal
	) {
		variables.memento['DateImported'] = arguments.mVal;
	}
	public boolean function hasDateImported() {
		return structKeyExists(variables.memento, 'DateImported');
	}


}
