component {

     public movieapp.com.model.services.CategoryService function init(
        required rcv.utils.Logger Logger
        , required rcv.utils.batman batman
     ) {
         structAppend(variables, arguments, true);
         return this;
     }


     /**
      * I return a new Category obj. 
      */
      public movieapp.com.model.Category function new(
        struct Args = {'id':0}
    ) {
        return new movieapp.com.model.Category(arguments.Args, variables.Logger);
    }

    /**
     * I persist a Category Object
     */
    public boolean function save(
        required movieapp.com.model.Category Category
    ) {
       var ret = false;

       return ret;
    }

    /**
     * I return an array of Category Objects
     */
    public array function list(
        struct sort = {}
    ) {
        var ret = [];

        return ret;
    }
    
    /**
     * I return an array of structs
     */
    public array function listForRest(
        struct sort = {}
    ) {
        var qExec=queryExecute(
            sql:"
                    select cat.id_cat, cat.cat_name, count(mov.id_movie) as numberOfMovies
                    from Category cat
                        left join MovieCategory movcat on cat.id_cat=movcat.id_cat
                        left join Movie mov on mov.id_movie = movcat.id_movie
                    GROUP BY cat.id_cat, cat.cat_name
                    ORDER BY numberOfMovies desc
                ");
        return variables.batman.convertQueryForJSONOutput(qExec, 'id_cat');
    }

    /**
     * I return an array of Category Objects for a Studio
     */
    public movieapp.com.model.Category[] function listByStudio(
        required movieapp.com.model.Studio Studio
        , struct sort = {}
    ) {
       var ret = [];

       return ret;
    }

       /**
     * I return an array of Category Objects for an actr
     */
    public movieapp.com.model.Category[] function listByactr(
        movieapp.com.model.actr actr
        , struct sort = {}
    ) {
       var ret = [];

       return ret;
    }

}