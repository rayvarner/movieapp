component {

     public movieapp.com.model.services.StudioService function init(
        required rcv.utils.Logger Logger
     ) {
         structAppend(variables, arguments, true);
         return this;
     }



     /**
      * I return a new Studio obj. 
      */
      public movieapp.com.model.Studio function new(
        struct Args = {'id':0}
    ) {
        return new movieapp.com.model.Studio(arguments.Args, variables.Logger);
    }

    /**
     * I persist a Studio Object
     */
    public boolean function save(
        required movieapp.com.model.Studio Studio
    ) {
       var ret = false;

       return ret;
    }

    /**
     * I return an array of Studio Objects
     */
    public array function list(
        struct sort = {}
    ) {
        var ret = [];

        return ret;
    }
}