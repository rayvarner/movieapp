component {

     public movieapp.com.model.services.actrservice function init(
        required rcv.utils.Logger Logger
     ) {
         structAppend(variables, arguments, true);
         return this;
     }



     /**
      * I return a new actr obj. 
      */
      public movieapp.com.model.actr function new(
        struct Args = {'id':0}
    ) {
        return new movieapp.com.model.actr(arguments.Args, variables.Logger);
    }

    /**
     * I persist a actr Object
     */
    public boolean function save(
        required movieapp.com.model.actr actr
    ) {
       var ret = false;

       return ret;
    }

    /**
     * I return an array of actr Objects
     */
    public array function list(
        struct sort = {}
    ) {
        var ret = [];

        return ret;
    }

    /**
     * I return an array of actr Objects
     */
    public movieapp.com.model.actr[] function listByCategory(
        required movieapp.com.model.Category[] Categorys
        , struct sort = {}
    ) {
        var ret = [];

        return ret;
    }    

}