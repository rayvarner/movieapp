component {

     public movieapp.com.model.services.MovieService function init(
        required rcv.utils.Logger Logger
        , required rcv.utils.batman batman
     ) {
         structAppend(variables, arguments, true);
         return this;
     }

     /**
      * I return a new Movie obj. 
      */
     public movieapp.com.model.Movie function new(
         struct myStruct = {}
     ) {
        var structData = {
            'id':0, 'title':'', 'meta_vr':'', 'dateProduced':'1999-12-31'
            , 'video_name':'', 'location_current':'', 'location_archive':'', 'graphic':''
            , 'studio':{} , 'actrs':[] , 'categorys':[]
        }
        structAppend(structData, arguments.myStruct, true);
         return new movieapp.com.model.Movie(
             myStruct = structData
             , Logger = variables.Logger
        );
     }

     /**
      * I persist a Movie Object
      */
     public boolean function save(
         required movieapp.com.model.Movie Movie
     ) {
        var ret = false;
        var qMovie=queryNew('');
        var qactr=queryNew('');
        var qCategory=queryNew('');

        try {
            cfstoredproc( procedure="sp_insert_movie" ) {
                cfprocparam( cfsqltype="cf_sql_varchar"     , value=arguments.Movie.getTitle() );
                cfprocparam( cfsqltype="cf_sql_varchar"     , value=arguments.Movie.getStudio().getName() );
                cfprocparam( cfsqltype="cf_sql_varchar"     , value=arguments.Movie.getvideo_name() );
                cfprocparam( cfsqltype="cf_sql_varchar"     , value=arguments.Movie.getgraphic() );
                cfprocparam( cfsqltype="cf_sql_date"     	, value=arguments.Movie.getDateImported() );
                cfprocparam( cfsqltype="cf_sql_varchar"     , value=arguments.Movie.getMeta_VR() );
                cfprocparam( cfsqltype="cf_sql_date"     	, value=arguments.Movie.getdateProduced() );
                cfprocparam( cfsqltype="cf_sql_varchar"     , value=arguments.Movie.getLocation_Current() );
                cfprocparam( cfsqltype="cf_sql_varchar"     , value=arguments.Movie.getLocation_archive() );
                cfprocresult( name="qMovie" );
            };

            for(var actr in arguments.Movie.getActrs()) {
                qactr=queryNew('');
                cfstoredproc( procedure="sp_insert_actr") {
                    cfprocparam( cfsqltype="cf_sql_varchar", value=actr.getNameCompact() );
                    //cfprocparam( cfsqltype="cf_sql_varchar", value=actr.getNameExpanded() );
                    //cfprocparam( cfsqltype="cf_sql_varchar", value=actr.getAlias1() );
                    //cfprocparam( cfsqltype="cf_sql_varchar", value=actr.getAlias2() ); 
                    cfprocresult( name="qactr" );
                };
                
                // insert the relationship
                 cfstoredproc( procedure="sp_link_movie_actr") {
                    cfprocparam( cfsqltype="cf_sql_integer", value=qMovie.id_movie );
                    cfprocparam( cfsqltype="cf_sql_integer", value=qactr.id_actr );
                    cfprocresult( name="success" );
                };
                
            }
 
             for(var Category in arguments.Movie.getCategorys() ) {
                qCategory=queryNew('');
                cfstoredproc( procedure="sp_insert_category" ) {
                    cfprocparam( cfsqltype="cf_sql_varchar", value=Category.getName() );
                    cfprocresult( name="qCategory" );
                };
                                
                // insert the relationship
                cfstoredproc( procedure="sp_link_movie_category") {
                    cfprocparam( cfsqltype="cf_sql_integer", value=qMovie.id_movie );
                    cfprocparam( cfsqltype="cf_sql_integer", value=qCategory.id_cat );
                    cfprocresult( name="success" );
                };
            } 
            ret = true;
        }
        catch(any e) {
            ret = false;
            variables.logger.logMe('==MovieService.save() ERROR: #e.message# #e.detail#', 'error');
        }
        

        return ret;
     }

     /**
      * I (will) return an array of Movie Objects
      * movieapp.com.model.Movie[]
      */
     public query function list(
         struct sort = {}
     ) {
         var ret = [];

         ret=queryExecute(
            sql:"
                select mov.id_movie
                , stu.studio_name
                , mov.movie_title
                , gra.graphic
                , vid.video_name
                , mov.meta_vr
                , mov.dateProduced 
                , vid.location_current
                , (STR(cat.id_cat) + '|' + cat.cat_name) as categorys
                , (STR(actr.id_actr) + '|' + actr.namecompact) as actrs
                from movie mov
                    inner join Studio stu				on mov.id_studio = stu.id_studio
                    left join Movieactr movactr		on mov.id_movie = movactr.id_movie
                    left join actr actr				on actr.id_actr = movactr.id_actr
                    left join MovieCategory movcat	on mov.id_movie = movcat.id_movie
                    left join Category cat			on cat.id_cat =		movcat.id_cat
                    inner join GraphicFile gra		on mov.id_movie = gra.id_movie
                    inner join VideoFile vid			on mov.id_movie = vid.id_movie
                order by mov.id_movie asc, stu.studio_name, mov.movie_title, cat.cat_name, actr.namecompact
                "
            // Lucee gives us some additional options which I'll leave out for the moment
            
            //, options:{returntype:"struct|array", columnKey:'id_movie'}
            //, params={title:{type:'varchar',value:'Susi'}}
        );

         return ret;
     }


     public array function listForCategoryRest(
         array cat_nameArray = []
     ) {
        var args = { 'sql':'', params:{} }; // argumentCollection to queryExecute
        args.sql = "
            SELECT 
              mov.id_movie as id
            , mov.movie_title as title
            , stu.studio_name as studio
            , gf.graphic
            , vid.video_name
            , ltrim(str(cat.id_cat) + '|' + cat.cat_name) as categorys
            , ltrim(str(actr.id_actr) + '|' + actr.namecompact) as actrs
            FROM Movie mov
                inner join MovieCategory movcat 	on mov.id_movie = movcat.id_movie
                inner JOIN Category cat 			on cat.id_cat = movcat.id_cat
                inner join MovieActr movactr 		on mov.id_movie = movactr.id_movie
                inner join Actr actr 				on actr.id_actr = movactr.id_actr
                inner join GraphicFile gf 			on mov.id_movie = gf.id_movie
                inner join VideoFile vid 			on mov.id_movie = vid.id_movie
                inner join Studio stu 			on mov.id_studio = stu.id_studio
            ";

        //  category arguments
        if(arrayLen(arguments.cat_nameArray)>=1) {

            for(var i=1;i<=arrayLen(arguments.cat_nameArray);i++) {
                if(i==1) {
                    args.sql &= "WHERE ";
                }
                else {
                    args.sql &= "AND ";
                }
                args.sql &= "mov.id_movie IN (
                                SELECT mov#i#.id_movie
                                FROM Movie mov#i#
                                    inner join MovieCategory movcat#i# 	on mov#i#.id_movie = movcat#i#.id_movie
                                    inner JOIN Category cat#i# 			on cat#i#.id_cat = movcat#i#.id_cat
                                WHERE cat#i#.cat_name=:cat_name#i#
                    )";
                structInsert(args.params, 'cat_name#i#', { type:'varchar', value: arguments.cat_nameArray[i] } );

            }

        }
        // add the order by
        args.sql &= "
        order by mov.id_movie, cat.cat_name, actr.namecompact";
        // execute
        var qExec=queryExecute(
            //argumentCollection = args
            sql=args.sql, params=args.params
            );

        // convert to the desired array
        return variables.batman.convertQueryForJSONOutput(qExec, 'id'
                , [   { 'col':'actrs'		, 'key':'id_actr'	} // col is the name of the column in the query
                    , { 'col':'categorys'	, 'key':'id_cat'	} // key is what YOU WANT to be the name of the result: "category":[{"id_cat":" 1001","val":"SciFi"}]
                ]);
     }



    /**
     * I return an array of Movie Objects for a Studio
     */
     public movieapp.com.model.Movie[] function listByStudio(
        required movieapp.com.model.Studio Studio
        , struct sort = {}
     ) {
        var ret = [];

        return ret;
     }

        /**
      * I return an array of Movie Objects for an Actr
      */
     public movieapp.com.model.Movie[] function listByActr(
         movieapp.com.model.Actr[] Actrs
         , struct sort = {}
     ) {
        var ret = [];

        return ret;
     }

    /**
     * I return an array of Movie Objects for a Category
     */
    public movieapp.com.model.Movie[] function listByCategory(
        required movieapp.com.model.Category[] Categorys
        , struct sort = {}
     ) {
        var ret = [];

        return ret;
     }

}