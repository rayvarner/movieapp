component extends="movieapp.com.model.beans.Movie" accessors=false {

	public movieapp.com.model.Movie function init(
        struct myStruct = {}
        , required rcv.utils.Logger Logger
        ) {
        variables.Logger = arguments.Logger;
		return super.init(myStruct=arguments.myStruct);
	}

}