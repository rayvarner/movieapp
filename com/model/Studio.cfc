component extends="movieapp.com.model.beans.Studio" accessors=false {
    
	public movieapp.com.model.Studio function init(
        struct myStruct = {}
        , required rcv.utils.Logger Logger
        ) {
        variables.Logger = arguments.Logger;
		return super.init(myStruct=arguments.myStruct);
	}

}