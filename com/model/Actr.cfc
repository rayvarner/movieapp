component extends="movieapp.com.model.beans.Actr" accessors=false {
    
	public movieapp.com.model.Actr function init(
        struct myStruct = {}
        , required rcv.utils.Logger Logger
        ) {
        variables.Logger = arguments.Logger;
		return super.init(myStruct=arguments.myStruct);
	}

}