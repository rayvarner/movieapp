component extends="movieapp.com.model.beans.Category" accessors=false {
    
	public movieapp.com.model.Category function init(
        struct myStruct = {}
        , required rcv.utils.Logger Logger
        ) {
        variables.Logger = arguments.Logger;
		return super.init(myStruct=arguments.myStruct);
	}

}